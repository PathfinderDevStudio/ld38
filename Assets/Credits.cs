﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class Credits : MonoBehaviour {

    private float offset;
    public float speed;
    public GUIStyle style;
    public Rect viewArea;
    public Texture logo;

    private void Start()
    {
        if (this.viewArea.width == 0.0f)
        {
            this.viewArea = new Rect(0.0f, 0.0f, Screen.width, Screen.height);
        }
        this.offset = this.viewArea.height;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    private void Update()
    {
        this.offset -= Time.deltaTime * this.speed;
    }

    private void OnGUI()
    {
        GUI.BeginGroup(this.viewArea);

        var position = new Rect(0, this.offset, this.viewArea.width, this.viewArea.height);
       
        GUI.Label(position, logo, this.style); 
        GUI.EndGroup();
    }

    public void GoToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void ClickExit()
    {
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #else
        Application.Quit();
        #endif
    }

    public void GoToSite()
    {
        Application.OpenURL("http://leipfertcomputinggamestudios.wordpress.com");
    }
}