﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CutScene : MonoBehaviour {

    public GameObject[] NavPoints;
    public GameObject ShuttleGO;
    public Canvas DialogCanvas;
    public Text DialogText;
    public GameObject Moon, Earth, Arks, Shuttle;
    public Camera MainCamera;
    public float speed;
    public TextAsset DialogTextFile;
    public Image DialogImageUI;
    public Sprite[] DialogImages;
    public Image BlackScreen;
    public AudioSource Radio;
    public AudioClip RadioStatic;
    private string[] DialogLines;
    private int Index = 1;
    private float startTime;
    private float JourneyLength;
    private GameObject CurrNavPoint;
    private GameObject NextNavPoint;
    private GameObject ArkNavPoint;

	// Use this for initialization
	void Start () {
        CurrNavPoint = NavPoints[0];
        NextNavPoint = NavPoints[1];
        ArkNavPoint = NavPoints[6];
        startTime = Time.time;
        JourneyLength = Vector3.Distance(CurrNavPoint.transform.position, NextNavPoint.transform.position);

        DialogCanvas.enabled = false;

        ReadTextFlie();

        StartCoroutine(PlayDialog());
	}
	
	// Update is called once per frame
	void Update () {
        float DistCovered = (Time.time - startTime) * speed;
        float FracJourney = DistCovered / JourneyLength;
        transform.position = Vector3.Lerp(CurrNavPoint.transform.position, NextNavPoint.transform.position, FracJourney);

        if (FracJourney >= 1 && NextNavPoint != NavPoints[7])
        {
            NavigateToNext();
        }

        if (NextNavPoint == NavPoints[7])
        {
            ShuttleGO.SetActive(false);
        }

        if (NextNavPoint == NavPoints[6])
        {
            transform.LookAt(Shuttle.transform);
        }
        else if (NextNavPoint == NavPoints[7])
        {
            transform.LookAt(Arks.transform);
        }
        else if (NextNavPoint == NavPoints[1] )
        {
            transform.LookAt(Moon.transform);
        }
        else if ( NextNavPoint == NavPoints[2] || NextNavPoint == NavPoints[3])
        {
            transform.LookAt(Earth.transform);
        }
        else if (NextNavPoint == NavPoints[4])
        {
            transform.LookAt(Arks.transform);
        }
        else if (NextNavPoint == NavPoints[5])
        {
            transform.LookAt(Shuttle.transform);
        }
	}

    void NavigateToNext()
    {
        CurrNavPoint = NextNavPoint;
        Index++;
        NextNavPoint = NavPoints[Index];
        startTime = Time.time;
        JourneyLength = Vector3.Distance(CurrNavPoint.transform.position, NextNavPoint.transform.position);
    }

    void ReadTextFlie()
    {
        if(DialogTextFile != null)
        {
            // Add each line of the text file to
            // the array using the new line
            // as the delimiter
            DialogLines = ( DialogTextFile.text.Split( '\n' ) );
        }
    }

    IEnumerator  PlayDialog()
    {
        Radio.clip = RadioStatic;

        yield return new WaitForSecondsRealtime(25.0f);

        //-----------Earth Control----------

        Radio.Play();

        DialogCanvas.enabled = true;

        DialogImageUI.sprite = DialogImages[0];

        DialogText.text = DialogLines[0];

        //---------Ark Discovery---------

        yield return new WaitForSecondsRealtime(10.0f);

        Radio.Play();

        DialogImageUI.sprite = DialogImages[1];

        DialogText.text = DialogLines[1];

        //------------Ark Discord-----------

        yield return new WaitForSecondsRealtime(10f);

        Radio.Play();

        DialogImageUI.sprite = DialogImages[2];

        DialogText.text = DialogLines[2];

        //-----------Ark Prophet--------

        yield return new WaitForSecondsRealtime(10.0f);

        Radio.Play();

        DialogImageUI.sprite = DialogImages[3];

        DialogText.text = DialogLines[3];

        //---------Ark Authoritris---------

        yield return new WaitForSecondsRealtime(15.0f);

        Radio.Play();

        DialogImageUI.sprite = DialogImages[4];

        DialogText.text = DialogLines[4];

        //---------Ark Authoritris---------

        yield return new WaitForSecondsRealtime(15.0f);

        Radio.Play();

        DialogImageUI.sprite = DialogImages[4];

        DialogText.text = DialogLines[5];


        //-----------Earth Control----------

        yield return new WaitForSecondsRealtime(8.0f);

        Radio.Play();

        DialogImageUI.sprite = DialogImages[0];

        DialogText.text = DialogLines[6];

        //-----------Earth Control----------

        yield return new WaitForSecondsRealtime(8.0f);

        Radio.Play();

        DialogImageUI.sprite = DialogImages[0];

        DialogText.text = DialogLines[7];

        yield return new WaitForSecondsRealtime(6.0f);
        FadeToBlack();

        yield return new WaitForSecondsRealtime(4.0f);

        LoadGame();
    }

    void LoadGame()
    {
        SceneManager.LoadScene("Game");
    }

    void FadeToBlack ()
    {
        BlackScreen.color = Color.black;
        BlackScreen.canvasRenderer.SetAlpha(0.0f);
        BlackScreen.CrossFadeAlpha (1.0f, 4.0f, false);
    }
}