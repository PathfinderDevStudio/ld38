﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    
    public AudioSource GameAudio;
    public AudioClip Alarm;
    public AudioClip GameMusic;
    public AudioClip ImSorry;
    public TextAsset DialogTextFile;
    public Canvas WarningScreen;
    public Canvas DialogScreen;
    public Image DialogImage;
    public Text DialogText;
    public Image BlackScreen;
    public Text WarningText;
    public Button CancelAlarmButton;
    public Text CancelAlarmButtonText;
    public Button NextPlanetButton;
    public Button InspectButton;
    public Light LightLeft, LightRight;
    public GameObject ArkAuthoritris, ArkDiscord, ArkDiscovery, ArkProphet, Planet;
    public Material[] PlanetMaterials;
    public GameObject ViewScreen;
    public Material PlanetCameraTexture, StarCameraTexture;
    public Sprite Character,PAL,PALRed;
    public Text TimerText, PlanetsVisitedText;
    private int PlanetMatIndex = 0;
    private bool AtArkAuthoritris, AtArkDiscord, AtArkDiscovery, AtArkProphet;
    private float AbortTimer = 5;
    private bool AlarmOn = true;
    private bool Abort = false;
    private bool inquired = false;
    private string[] DialogLines;
    private float duration = 2.0F;
    private Renderer ViewScreenRenderer;
    private Renderer PlanetRenderer;
    private int PlanetsVisited = 0;
    private float TimerSec = 0;
    private float TimerMin = 0;
    private string Designation;
    private string Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private string[] inquryArr = {". There are no signs of life." , ". This is not a habitable planet." , ". No data about this planet." , ". This planet has an extremely Toxit atmosphere."};


	// Use this for initialization
	void Start () {
        WarningScreen.enabled = true;

        WarningText.text = "Catastrophic Failure On Ark Authoritris!";

        PlanetsVisitedText.text = "0";

        DialogScreen.enabled = false;
        InspectButton.interactable = false;
        NextPlanetButton.interactable = false;

        ReadTextFlie();

        ArkAuthoritris.SetActive(true);
        ArkDiscord.SetActive(false);
        ArkDiscovery.SetActive(false);
        ArkProphet.SetActive(false);
        Planet.SetActive(false);

        AtArkAuthoritris = true;

        GameAudio.clip = Alarm;

        GameAudio.Play();

        StartCoroutine(WakeUp());
	}

    void ReadTextFlie()
    {
        if(DialogTextFile != null)
        {
            // Add each line of the text file to
            // the array using the new line
            // as the delimiter
            DialogLines = ( DialogTextFile.text.Split( '\n' ) );
        }
    }
	
	// Update is called once per frame
	void Update () {

        UpdateTimerUI();

        if (Abort == true)
        {
            AbortTimer -= Time.deltaTime;
            if (AbortTimer >= 2)
            {
                WarningText.text = "You have " + ((int)AbortTimer).ToString() + " Seconds to Confim Aborting!";
            }
            else if (AbortTimer < 2)
            {
                WarningText.text = "You have " + ((int)AbortTimer).ToString() + " Second to Confim Aborting!";
            }
        }

        if (AlarmOn == true)
        {
            float phi = Time.time / duration * 2 * Mathf.PI;
            float amplitude = Mathf.Cos(phi) * 0.5F + 0.5F;
            LightLeft.intensity = amplitude;
            LightRight.intensity = 1-amplitude;
        }
	}

    public void AbortGame()
    {
        if (Abort == true)
        {
            FadeToBlack(5);
            SceneManager.LoadScene("Credits");
        }
        else
        {
            Abort = true;

            WarningScreen.enabled = true;

            StartCoroutine(ResetAbort());
        }
    }

    IEnumerator ResetAbort()
    {
        yield return new WaitForSecondsRealtime(5);
        Abort = false;
        WarningScreen.enabled = false;
        AbortTimer = 5;
    }

    void FadeFromBlack(int time)
    {
        BlackScreen.color = Color.black;
        BlackScreen.canvasRenderer.SetAlpha(1.0f);
        BlackScreen.CrossFadeAlpha(0.0f, time, false);
    }

    void FadeToBlack(int time)
    {
        BlackScreen.color = Color.black;
        BlackScreen.canvasRenderer.SetAlpha(0.0f);
        BlackScreen.CrossFadeAlpha(1.0f, time, false);
    }

    IEnumerator WakeUp()
    {
        FadeFromBlack(2);
        yield return new WaitForSecondsRealtime(2);
        FadeToBlack(2);
        yield return new WaitForSecondsRealtime(2);
        FadeFromBlack(2);

        yield return new WaitForSecondsRealtime(3);

        BlackScreen.enabled = false;
    }

    public void CancelAlarm()
    {
        GameAudio.Stop();

        Color CanceledColor = new Color();
        ColorUtility.TryParseHtmlString("#A7A7A74A", out CanceledColor);

        CancelAlarmButton.interactable = false;

        CancelAlarmButtonText.color = CanceledColor;

        AlarmOn = false;

        LightLeft.color = Color.white;
        LightRight.color = Color.white;
        LightLeft.intensity = 1;
        LightRight.intensity = 1;

        WarningScreen.enabled = false;

        GameAudio.clip = GameMusic;
        GameAudio.Play();

        StartCoroutine(Dialog());
    }

    IEnumerator Dialog()
    {
        yield return new WaitForSecondsRealtime(1);
        DialogScreen.enabled = true;
        DialogImage.sprite = Character;
        DialogText.text = DialogLines[0];
        yield return new WaitForSecondsRealtime(7);
        DialogImage.sprite = PAL;
        DialogText.text = DialogLines[1];
        yield return new WaitForSecondsRealtime(7);
        DialogImage.sprite = Character;
        DialogText.text = DialogLines[2];
        yield return new WaitForSecondsRealtime(7);
        DialogImage.sprite = PAL;
        DialogText.text = DialogLines[3];
        yield return new WaitForSecondsRealtime(7);
        DialogImage.sprite = Character;
        DialogText.text = DialogLines[4];
        yield return new WaitForSecondsRealtime(7);
        DialogImage.sprite = PAL;
        DialogText.text = DialogLines[5];
        yield return new WaitForSecondsRealtime(7);
        DialogImage.sprite = Character;
        DialogText.text = DialogLines[6];
        yield return new WaitForSecondsRealtime(7);
        DialogImage.sprite = PAL;
        DialogText.text = DialogLines[7];
        yield return new WaitForSecondsRealtime(7);
        DialogImage.sprite = Character;
        DialogText.text = DialogLines[8];
        yield return new WaitForSecondsRealtime(7);
        DialogImage.sprite = PAL;
        DialogText.text = DialogLines[9];
        yield return new WaitForSecondsRealtime(7);
        DialogImage.sprite = Character;
        DialogText.text = DialogLines[10];
        yield return new WaitForSecondsRealtime(7);

        DialogScreen.enabled = false;
        InspectButton.interactable = true;
        NextPlanetButton.interactable = true;


    }

    public void InquireAboutPlanet()
    {
        //Display Dialog Screen and Display info
        StartCoroutine(Inquire());
    }

    IEnumerator Inquire()
    {
        if (AtArkAuthoritris)
        {
            yield return new WaitForSecondsRealtime(2);
            DialogScreen.enabled = true;
            DialogImage.sprite = PAL;
            DialogText.text = "This is what remains of the Ark Authoritris. There was a sudden loss of power. Followed by numerous explosions. There are no signs of life left on board";
            yield return new WaitForSecondsRealtime(7);
        }
        else if (AtArkDiscord)
        {
            yield return new WaitForSecondsRealtime(2);
            DialogScreen.enabled = true;
            DialogImage.sprite = PAL;
            DialogText.text = "This is what remains of the Ark Discord. There are no signs of life left on board";
            yield return new WaitForSecondsRealtime(5);
        }
        else if (AtArkDiscovery)
        {
            yield return new WaitForSecondsRealtime(2);
            DialogScreen.enabled = true;
            DialogImage.sprite = PAL;
            DialogText.text = "This is what remains of the Ark Discovery. This Ark is no longer capable of making any discoveries. No survivors.";
            yield return new WaitForSecondsRealtime(7);
        }
        else if (AtArkProphet)
        {
            yield return new WaitForSecondsRealtime(2);
            DialogScreen.enabled = true;
            DialogImage.sprite = PAL;
            DialogText.text = "This is what remains of the Ark Prophet. There seems to be no signs of life abord the ark.";
            yield return new WaitForSecondsRealtime(5);
        }
        else
        {
            if (inquired == false)
            {   
                DesignationGenerator();

                yield return new WaitForSecondsRealtime(2);
                DialogScreen.enabled = true;
                DialogImage.sprite = Character;
                DialogText.text = "PAL what can you tell me about this planet?";
                yield return new WaitForSecondsRealtime(5);
                DialogImage.sprite = PAL;
                DialogText.text = "This is " + Designation + inquryArr[Random.Range(0, inquryArr.Length)];
                yield return new WaitForSecondsRealtime(7);

                inquired = true;
            }
            else
            {
                yield return new WaitForSecondsRealtime(2);
                DialogScreen.enabled = true;
                DialogImage.sprite = Character;
                DialogText.text = "PAL what can you tell me about this planet?";
                yield return new WaitForSecondsRealtime(5);
                DialogImage.sprite = PAL;
                DialogText.text = "This is " + Designation + " I already told you.";
                yield return new WaitForSecondsRealtime(5);
            }
        }

        DialogScreen.enabled = false;
    }


    void DesignationGenerator()
    {
        Designation = "P" + Random.Range(0, 9) + Alphabet[Random.Range(0, Alphabet.Length)]+ "-" + Random.Range(0, 9) + Random.Range(0, 9) + Random.Range(0, 9);
            
    }

    public void NextPlanet()
    {
        //To The Next Planet
        StartCoroutine(Travel());
    }

    IEnumerator Travel()
    {
        if (AtArkProphet)
        {
            StartCoroutine(EndGame());
        }
        else 
        {
            if (AtArkAuthoritris || AtArkDiscord || AtArkDiscovery)
            {
                ArkAuthoritris.SetActive(false);
                ArkDiscord.SetActive(false);
                ArkDiscovery.SetActive(false);
                ArkProphet.SetActive(false);

                Planet.SetActive(true);

                AtArkAuthoritris = false;
                AtArkDiscord = false;
                AtArkDiscovery = false;
            }


            PlanetsVisited++;
            PlanetsVisitedText.text = PlanetsVisited.ToString();


            if (PlanetsVisited == 10)
            {
                AtArkDiscord = true;
            }
            else if (PlanetsVisited == 20)
            {
                AtArkDiscovery = true;
            }
            else if (PlanetsVisited == 30)
            {
                AtArkProphet = true;
            }


            ViewScreenRenderer = ViewScreen.GetComponentInParent<Renderer>();

            ViewScreenRenderer.material = StarCameraTexture;

            PlanetRenderer = Planet.GetComponentInParent<Renderer>();

            PlanetMatIndex = Random.Range(0, PlanetMaterials.Length);

            if (AtArkDiscord)
            {
                Planet.SetActive(false);

                ArkDiscord.SetActive(true);

                yield return new WaitForSecondsRealtime(2);
            }
            else if (AtArkDiscovery)
            {
                Planet.SetActive(false);

                ArkDiscovery.SetActive(true);

                yield return new WaitForSecondsRealtime(2);
            }
            else if (AtArkProphet)
            {
                Planet.SetActive(false);

                ArkProphet.SetActive(true);

                yield return new WaitForSecondsRealtime(2);
            }
            else
            {
                yield return new WaitForSecondsRealtime(2);

                Debug.Log(PlanetMatIndex.ToString());

                PlanetRenderer.material = PlanetMaterials[PlanetMatIndex];
            }
            ViewScreenRenderer.material = PlanetCameraTexture;

            inquired = false;
        }
    }

    void UpdateTimerUI()
    {
        TimerSec += Time.deltaTime;

        if (TimerSec < 10)
        {
            TimerText.text = (int)TimerMin + ":0" + (int)TimerSec;
        }
        else
        {
            TimerText.text = (int)TimerMin + ":" + (int)TimerSec;
        }

        if (TimerSec >= 60)
        {
            TimerMin += 1;
            TimerSec = 0;
        }
    }

    IEnumerator EndGame()
    {
        yield return new WaitForSecondsRealtime(2);
        DialogScreen.enabled = true;
        GameAudio.clip = ImSorry;
        GameAudio.Play();
        DialogImage.sprite = PALRed;
        DialogText.text = "I'm sorry, Dave. I'm afraid I can't do that.";
        yield return new WaitForSecondsRealtime(3);
        DialogText.text = "Powering Down Shuttle.";
        yield return new WaitForSecondsRealtime(3);
        DialogText.text = "Good Bye Dave.";
        LightLeft.intensity = .5f;
        LightRight.intensity = .5f;
        LightLeft.color = Color.red;
        LightRight.color = Color.red;

        BlackScreen.enabled = true;

        FadeToBlack(5);

        yield return new WaitForSecondsRealtime(7);

        SceneManager.LoadScene("Credits");
    }
}
